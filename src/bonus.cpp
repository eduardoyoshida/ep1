#include <iostream>
#include "bonus.hpp"
#include "gameobject.hpp"

using namespace std;

Bonus::Bonus(int positionY, int positionX){
	this->setScore(1);
	this->setSprite('*');
	this->setPositionX(positionX);
	this->setPositionY(positionY);
}

void Bonus::setScore(int score){
	this->score = score;
}

int Bonus::getScore(){
	return this->score;
}
