#include <iostream>
#include "trap.hpp"
#include "gameobject.hpp"

using namespace std;

Trap::Trap(int damage, char sprite, int positionY, int positionX){
	this->setDamage(damage);
	this->setSprite(sprite);
	this->setPositionY(positionY);
	this->setPositionX(positionX);
}

Trap::~Trap(){
}

void Trap::setDamage(int damage){
	this->damage = damage;
}

int Trap::getDamage(){
	return this->damage;
}
