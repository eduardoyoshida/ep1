#include <iostream>
#include "gameobject.hpp"

using namespace std;

GameObject::GameObject(){}

GameObject::~GameObject(){}

void GameObject::setPositionX(int positionX){
	this->positionX = positionX;
}
int GameObject::getPositionX(){
	return this->positionX;
}
void GameObject::setPositionY(int positionY){
	this->positionY = positionY;
}
int GameObject::getPositionY(){
	return this->positionY;
}
void GameObject::setSprite(char sprite){
	this->sprite = sprite;
}
char GameObject::getSprite(){
	return this->sprite;
}
