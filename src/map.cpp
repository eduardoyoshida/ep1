#include <iostream>
#include <string>
#include "map.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>
#include "gameobject.hpp"

using namespace std;

Map::Map(){}
Map::~Map(){}

void Map::setCoordinate(){
  ifstream map("src//map.txt");
  string aux;

  for(int i=0; i<20; i++){
    getline(map, aux);
    for(int j=0; j<50; j++){
      this->coordinate[j][i] = aux[j];
    }
  }

  map.close();
}

char Map::getCoordinate(int j, int i){
  return this->coordinate[j][i];
}

void Map::addElement(GameObject * gameobj){//Método utilizado para adicionar traps/bonus
  if(this->coordinate[gameobj->getPositionY()][gameobj->getPositionX()] != '='){
    this->coordinate[gameobj->getPositionY()][gameobj->getPositionX()] = gameobj->getSprite();
  }
}

void Map::removeElement(int positionY, int positionX){
  if(this->coordinate[positionY][positionX] != '='){
    this->coordinate[positionY][positionX] = ' ';
  }
}
