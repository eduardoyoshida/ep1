#include <iostream>
#include <string>
#include <stdlib.h>
#include <ncurses.h>

#include "trap.hpp"
#include "map.hpp"
#include "player.hpp"
#include "draw.hpp"
#include "bonus.hpp"

using namespace std;



int main(){

	Map * mapa = new Map();
	mapa->setCoordinate();
	Draw * draw = new Draw();
	Player * player = new Player(2, 2, '@');
	char direction;
	Trap * trap1 = new Trap(0, 'X', 1, 2);
	Trap * trap2 = new Trap(0, 'X', 2, 4);
	Trap * trap3 = new Trap(0, 'X', 1, 6);
	Trap * trap4 = new Trap(0, 'X', 2, 8);
	Trap * trap5 = new Trap(0, 'X', 1, 10);
	Trap * trap6 = new Trap(0, 'X', 20, 18);
	Trap * trap7 = new Trap(0, 'X', 21, 16);


	Bonus * bonus1 = new Bonus(6,2);
	Bonus * bonus2 = new Bonus(18, 7);
	Bonus * bonus3 = new Bonus(48, 12);
	Bonus * bonus4 = new Bonus(22, 7);
	Bonus * bonus5 = new Bonus(19, 18);
	mapa->addElement(trap1);
	mapa->addElement(trap2);
	mapa->addElement(trap3);
	mapa->addElement(trap4);
	mapa->addElement(trap5);
	mapa->addElement(trap6);
	mapa->addElement(trap7);
	//As traps e bonus foram instanciados manualmente devido a complexidade de se gerar aleatoriamente
	//Afinal seria possível que traps geradas aleatoriamente bloqueassem o caminho
	mapa->addElement(bonus1);
	mapa->addElement(bonus2);
	mapa->addElement(bonus3);
	mapa->addElement(bonus4);
	mapa->addElement(bonus5);

	while(TRUE){
			initscr();
			clear();
			curs_set(0);
			keypad(stdscr, TRUE);
			noecho();

			draw->draw(mapa);
			draw->draw(player);
			draw->drawInfo(player);

			refresh();
			direction = getch();
			player->move(direction, mapa);//O colisor é acionado dentro do método move
			endwin();
			if(player->getAlive() == false){
				draw->drawDefeat();
				break;
			}
			if(player->getWinner() == true){
				draw->drawWin(player);
				break;
			}
	}
	return 0;
}
