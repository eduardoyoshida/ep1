#include <iostream>
#include "player.hpp"
#include "colisor.hpp"
#include "map.hpp"
#include <ncurses.h>

using namespace std;

Player::Player(int positionY, int positionX, char sprite){
	this->setPositionY(positionY);
	this->setPositionX(positionX);
	this->setSprite(sprite);
	this->setAlive(true);
	this->setWinner(false);
	this->setScore(0);
	this->setSteps(0);
};
//Player::~Player(){};

void Player::setAlive(bool alive){
	this->alive = alive;
}
bool Player::getAlive(){
	return this->alive;
}
void Player::setScore(int score){
	this->score = score;
}
int Player::getScore(){
	return this->score;
}
void 	Player::setWinner(bool winner){
	this->winner = winner;
}
bool Player::getWinner(){
	return this->winner;
}

void Player::setSteps(int steps){
	this->steps = steps;
}

int Player::getSteps(){
	return this->steps;
}

void Player::move(char movement, Map * map){
	Colisor * colisor = new Colisor();
	switch(movement){
		case 'w':
				if(!(colisor->isMapCollision(this->getPositionY(), this->getPositionX()-1, map))){
					this->setPositionX(this->getPositionX() - 1);//efetua movimento caso não haja colisão com as paredes
					this->setSteps(this->getSteps()+1);//contador de passos
				}
				if(colisor->isObjCollision(this->getPositionY(), this->getPositionX()-1, map)){
					if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'X')//trata das interações do player com os objetos onde há colisão
				    this->setAlive(false);
				  if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == '*')
				    this->setScore(this->getScore()+1);
					if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'o')
						this->setWinner(true);
					map->removeElement(this->getPositionY(), this->getPositionX());
				}
		break;
		case 'a':
			if(!(colisor->isMapCollision(this->getPositionY()-1, this->getPositionX(), map))){
				this->setPositionY(this->getPositionY() - 1);
				this->setSteps(this->getSteps()+1);
			}
			if(colisor->isObjCollision(this->getPositionY(), this->getPositionX(), map)){
				if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'X')
			    this->setAlive(false);
			  if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == '*')
			    this->setScore(this->getScore()+1);
				if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'o')
					this->setWinner(true);
				map->removeElement(this->getPositionY(), this->getPositionX());
			}
			break;
		case 's':
			if(!(colisor->isMapCollision(this->getPositionY(), this->getPositionX()+1, map))){
				this->setPositionX(this->getPositionX() + 1);
				this->setSteps(this->getSteps()+1);
			}
			if(colisor->isObjCollision(this->getPositionY(), this->getPositionX(), map)){
				if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'X')
			    this->setAlive(false);
			  if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == '*')
			    this->setScore(this->getScore()+1);
				if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'o')
					this->setWinner(true);
					map->removeElement(this->getPositionY(), this->getPositionX());
			}
			break;
		case 'd':
			if(!(colisor->isMapCollision(this->getPositionY()+1, this->getPositionX(), map))){
				this->setPositionY(this->getPositionY() + 1);
				this->setSteps(this->getSteps()+1);
			}
			if(colisor->isObjCollision(this->getPositionY(), this->getPositionX()-1, map)){
				if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'X')
			    this->setAlive(false);
			  if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == '*')
			    this->setScore(this->getScore()+1);
			  if(map->getCoordinate(this->getPositionY(), this->getPositionX()) == 'o')
					this->setWinner(true);
				map->removeElement(this->getPositionY(), this->getPositionX());
			}
				break;
		default:
			break;
	}
	delete(colisor);
}
