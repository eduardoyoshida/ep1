#include "colisor.hpp"
#include <iostream>
#include "map.hpp"
#include "player.hpp"
using namespace std;

Colisor::Colisor(){}

Colisor::~Colisor(){
}

bool Colisor::isMapCollision(int newPositionY, int newPositionX, Map * map){
  if(map->getCoordinate(newPositionY, newPositionX) == '=')
    return true;
  return false;
}

bool Colisor::isObjCollision(int newPositionY, int newPositionX, Map * map){
  if(map->getCoordinate(newPositionY, newPositionX) != ' ' ||map->getCoordinate(newPositionY, newPositionX) != '=' )
    return true;
  return false;
  }
