#include <iostream>
#include <string>
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>

#include "gameobject.hpp"
#include "map.hpp"
#include "draw.hpp"
#include "player.hpp"

Draw::Draw(){}

Draw::~Draw(){}

void Draw::draw(Map * map){
  for(int i=0; i<20; i++){
    for(int j=0; j<50; j++){
      printw("%c", map->getCoordinate(j, i));
      if(j>=49){
        printw("\n");
      }
    }
  }
}
//Sobrecarga do método draw
void Draw::draw(GameObject * gameobj){
  move(gameobj->getPositionX(), gameobj->getPositionY());
  printw("%c", gameobj->getSprite());
}

void Draw::drawInfo(Player * player){
  move(2, 55);
  printw("Pontuação: %i\n", player->getScore());
  move(3, 55);
  printw("Passos: %i", player->getSteps());
  move(4, 55);
  printw("X = armadilhas");
  move(5, 55);
  printw("* = bonus ");
  move(6, 55);
  printw("o = saída");
  if(player->getWinner() == true)
    printw("Venceu");
  if(player->getWinner() == false)
    printw("Perdeu");
}

void Draw::drawWin(Player * player){
  initscr();
  clear();
  printw("Parabéns\n");
  printw("Seus status: \n");
  printw("Pontuação: %i \n", player->getScore());
  printw("Passos: %i \n", player->getSteps());
  printw("Pressione uma tecla para continuar...");
  getch();
  endwin();
}

void Draw::drawDefeat(){
  initscr();
  clear();
  printw("Você perdeu\n");
  printw("Pressione uma tecla para continuar...");
  getch();
  endwin();
}
