#ifndef COLISOR_HPP
#define COLISOR_HPP

#include "map.hpp"
#include "player.hpp"

class Colisor{
private:

public:
  Colisor();
  ~Colisor();
  bool isMapCollision(int newPositionY, int newPositionX, Map * map);//retorna true se há colisão com paredes do mapa
  bool isObjCollision(int newPositionY, int newPOsitionX, Map * map);//retorna true se há colisão com objetos
};
#endif
