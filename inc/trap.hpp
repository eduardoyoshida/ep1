#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameobject.hpp"

class Trap : public GameObject{

	private:
		int damage;

	public:
		Trap(int damage, char sprite, int positionY, int positionX);
		~Trap();
		void setDamage(int damage);
		int getDamage();
};
#endif
