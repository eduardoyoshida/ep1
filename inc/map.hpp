#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <string>
#include "gameobject.hpp"
using namespace std;

class Map {

  private:
    char coordinate[50][20];

  public:
    Map();
    ~Map();

    void setCoordinate();//carrega o mapa do txt para o array
    char getCoordinate(int i, int j);//retorna o caractere nas coordenadas i,j do mapa

    void addElement(GameObject * gameobject);//métodos para adicionar/remover elementos do mapa
    void removeElement(int positionY, int positionX);
};
#endif
