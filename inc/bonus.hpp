#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameobject.hpp"

class Bonus : public GameObject{

	private:
		int score;
	public:
		Bonus(int positionY, int positionX);
		~Bonus();
		void setScore(int score);
		int getScore();
};
#endif
