#ifndef DRAW_HPP
#define DRAW_HPP

#include "map.hpp"
#include "gameobject.hpp"
#include "player.hpp"

class Draw{
private:

public:
  Draw();
  ~Draw();

  void draw(Map *  map);//método para desenhar o mapa
  void draw(GameObject * gameobj);//método utilizado para desenhar objetos do jogo -  sobrecarga
  void drawInfo(Player * player);//método para desenhar info
  void drawWin(Player * player);//desenhar status/tela de vitória
  void drawDefeat();
};
#endif
