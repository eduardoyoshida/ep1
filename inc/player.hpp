#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "map.hpp"
#include "gameobject.hpp"

class Player : public GameObject {

	private:
		bool alive;
		int score;
		int steps;
		bool winner;
		char sprite;

	public:
		Player(int positionY, int positionX, char sprite);
		~Player();

		void setAlive(bool alive);
		bool getAlive();
		void setScore(int score);
		int getScore();
		void setWinner(bool winner);
		bool getWinner();
		void setSteps(int steps);
		int getSteps();

		void move(char movement, Map * map);//Movimento do player, utiliza métodos do colisor	
};

#endif
