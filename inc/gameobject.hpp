#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

class GameObject {

	private:
		int positionX;
		int positionY;
		char sprite;
		char type;
	public:
		GameObject();
		~GameObject();

		void setPositionX(int PositionX);
		int getPositionX();
		void setPositionY(int PositionY);
		int getPositionY();
		void setSprite(char sprite);
		char getSprite();
};
#endif
